﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace senso_pop
{
    public partial class frm_main : Form
    {

        pessoas[] habitantes = new pessoas [1000];        

        int cont;

        public frm_main()
        {
            InitializeComponent();
        }
        public struct pessoas
        {
            private int id;
            private string nome;
            private int idade;
            private double renda;
            private string estadoCivil;
            private int numFilhos;

            public int getId()
            {
                return id;
            }

            public void setId(int id)
            {
                this.id = id;
            }

            public string getNome()
            {
                return nome;
            }

            public void setNome(string nome)
            {
                this.nome = nome;
            }

            public int getIdade()
            {
                return idade;
            }

            public void setIdade(int idade)
            {
                this.idade = idade;
            }

            public double getRenda()
            {
                return renda;
            }

            public void setRenda(double renda)
            {
                this.renda = renda;
            }

            public string getEstadoCivil()
            {
                return estadoCivil;
            }
            
            public void setEstadoCivil(string estadoCivil)
            {
                this.estadoCivil = estadoCivil;
            }

            public int getNumFilhos()
            {
                return numFilhos;
            }

            public void setNumFilhos(int numFilhos)
            {
                this.numFilhos = numFilhos;
            }
        }
        private void frm_main_Load(object sender, EventArgs e)
        {
            gpb_pessoa.Hide();
            lbl_erro002.Hide();
        }

        private void btn_sair_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            gpb_cidade.Show();
            gpb_pessoa.Hide();

            int numTotalFilhos = 0;
            double salarioTotal = 0;
            int Solteiros = 0;
            int Casados = 0;
            int Viuvos = 0;
            txt_MSporFilhos.Text = "0";
            

            for (int i = 0; i <= cont; i++)
            {
                numTotalFilhos = numTotalFilhos + habitantes[i].getNumFilhos();
                salarioTotal = salarioTotal + habitantes[i].getRenda();
            }

            for (int i = 0; i <= cont; i++)
            {
                if(habitantes[i].getEstadoCivil() == "Solteiro(a)")
                {
                    Solteiros++;
                }else if(habitantes[i].getEstadoCivil() == "Casado(a)")
                {
                    Casados++;
                }
                else if(habitantes[i].getEstadoCivil() == "Viúvo(a)")
                {
                    Viuvos++;
                }
                else
                {
                    
                }
            }



            txt_populacao.Text = cont.ToString();
            txt_MFporPessoa.Text = (Convert.ToDouble(numTotalFilhos)/Convert.ToDouble(cont)).ToString();
            txt_MSalarial.Text = (Convert.ToDouble(salarioTotal) / Convert.ToDouble(cont)).ToString();
            txt_PessoasSolteiros.Text = Solteiros.ToString();
            txt_PessoasCasados.Text = Casados.ToString();
            txt_PessoasViúvos.Text = Viuvos.ToString();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            gpb_cidade.Hide();
            gpb_pessoa.Show();
        }

        private void btn_limpar_Click(object sender, EventArgs e)
        {
            limparFormulario();
        }

        public void limparFormulario()
        {
            txt_id.Text = null;
            txt_idade.Text = null;
            txt_nome.Text = null;
            txt_numFilhos.Text = null;
            txt_renda.Text = null;
            rbtn_casado.Checked = false;
            rbtn_solteiro.Checked = false;
            rbtn_viuvo.Checked = false;
        }

        private void btn_cadastrar_Click(object sender, EventArgs e)
        {      
            int n = habitantes.Length;
            if (validarCampos() == true)
            {
                if (verificarId() == true)
                {
                    habitantes[cont].setId(Convert.ToInt32(txt_id.Text));
                    habitantes[cont].setNome(txt_nome.Text);
                    string estadocivil = estCivil();
                    habitantes[cont].setEstadoCivil(estadocivil);
                    habitantes[cont].setIdade(Convert.ToInt32(txt_idade.Text));
                    habitantes[cont].setNumFilhos(Convert.ToInt32(txt_numFilhos.Text));
                    habitantes[cont].setRenda(Convert.ToDouble(txt_renda.Text));
                    cont++;                    
                }
                else
                {
                    lbl_erro002.Show();
                    txt_id.Focus();
                }
            }
            else
            {
                MessageBox.Show("Preencha todos os campos para realizar o cadastro.", "Erro003");
            }

            limparFormulario();
        }

        public bool validarCampos()
        {
            if (txt_id.Text == null || txt_idade.Text == null || txt_nome.Text == null || txt_numFilhos.Text == null || txt_renda.Text == null)
            {
                return false;
            }
            else
            {
                if (rbtn_casado.Checked == false && rbtn_solteiro.Checked == false && rbtn_viuvo.Checked == false)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        public string estCivil()
        {
            if (rbtn_casado.Checked == true)
            {
                return "Casado(a)";
            }
            else if (rbtn_solteiro.Checked == true)
            {
                return "Solteiro(a)";
            }
            else if (rbtn_viuvo.Checked == true)
            {
                return "Viúvo(a)";
            }
            return null;
        }

        public bool verificarId()
        {
            for (int i = 0; i < habitantes.Length; i++)
            {
                int id = Convert.ToInt32(txt_id.Text);
                if (id == habitantes[i].getId())
                {
                    return false;
                }
            }
            return true;
        }

        private void btn_buscar_Click(object sender, EventArgs e)
        {
            double SomaporFilho = 0;
            double aux = 0;

            for(int i = 0; i <= cont; i++)
            {
                if (habitantes[i].getNumFilhos() == Convert.ToInt32(txt_Nfilhos.Text))
                {
                    SomaporFilho += habitantes[i].getRenda();
                    aux++;
                }
            }
            txt_MSporFilhos.Text = (SomaporFilho / aux).ToString();
        }
    }
}
