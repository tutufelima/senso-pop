﻿namespace senso_pop
{
    partial class frm_main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_main));
            this.button1 = new System.Windows.Forms.Button();
            this.btn_sair = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.gpb_cidade = new System.Windows.Forms.GroupBox();
            this.lbl_MSalarial = new System.Windows.Forms.Label();
            this.txt_MFporPessoa = new System.Windows.Forms.TextBox();
            this.lbl_MFporPessoa = new System.Windows.Forms.Label();
            this.txt_populacao = new System.Windows.Forms.TextBox();
            this.lbl_populacao = new System.Windows.Forms.Label();
            this.gpb_pessoa = new System.Windows.Forms.GroupBox();
            this.btn_limpar = new System.Windows.Forms.Button();
            this.btn_cadastrar = new System.Windows.Forms.Button();
            this.lbl_erro002 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.rbtn_viuvo = new System.Windows.Forms.RadioButton();
            this.rbtn_casado = new System.Windows.Forms.RadioButton();
            this.rbtn_solteiro = new System.Windows.Forms.RadioButton();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_numFilhos = new System.Windows.Forms.TextBox();
            this.txt_renda = new System.Windows.Forms.TextBox();
            this.txt_idade = new System.Windows.Forms.TextBox();
            this.txt_nome = new System.Windows.Forms.TextBox();
            this.txt_id = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_MSalarial = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txt_PessoasSolteiros = new System.Windows.Forms.TextBox();
            this.txt_PessoasCasados = new System.Windows.Forms.TextBox();
            this.txt_PessoasViúvos = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txt_Nfilhos = new System.Windows.Forms.TextBox();
            this.btn_buscar = new System.Windows.Forms.Button();
            this.txt_MSporFilhos = new System.Windows.Forms.TextBox();
            this.gpb_cidade.SuspendLayout();
            this.gpb_pessoa.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Black;
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(12, 197);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(108, 46);
            this.button1.TabIndex = 0;
            this.button1.Text = "Cidade";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btn_sair
            // 
            this.btn_sair.BackColor = System.Drawing.Color.Black;
            this.btn_sair.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_sair.FlatAppearance.BorderSize = 0;
            this.btn_sair.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_sair.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_sair.ForeColor = System.Drawing.Color.White;
            this.btn_sair.Location = new System.Drawing.Point(12, 426);
            this.btn_sair.Name = "btn_sair";
            this.btn_sair.Size = new System.Drawing.Size(108, 62);
            this.btn_sair.TabIndex = 1;
            this.btn_sair.Text = "Sair";
            this.btn_sair.UseVisualStyleBackColor = false;
            this.btn_sair.Click += new System.EventHandler(this.btn_sair_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Black;
            this.button2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(12, 249);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(108, 46);
            this.button2.TabIndex = 2;
            this.button2.Text = "Pessoas";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // gpb_cidade
            // 
            this.gpb_cidade.BackColor = System.Drawing.Color.Transparent;
            this.gpb_cidade.Controls.Add(this.txt_MSporFilhos);
            this.gpb_cidade.Controls.Add(this.btn_buscar);
            this.gpb_cidade.Controls.Add(this.txt_Nfilhos);
            this.gpb_cidade.Controls.Add(this.label15);
            this.gpb_cidade.Controls.Add(this.label14);
            this.gpb_cidade.Controls.Add(this.txt_PessoasViúvos);
            this.gpb_cidade.Controls.Add(this.txt_PessoasCasados);
            this.gpb_cidade.Controls.Add(this.txt_PessoasSolteiros);
            this.gpb_cidade.Controls.Add(this.label13);
            this.gpb_cidade.Controls.Add(this.label12);
            this.gpb_cidade.Controls.Add(this.label11);
            this.gpb_cidade.Controls.Add(this.label10);
            this.gpb_cidade.Controls.Add(this.label9);
            this.gpb_cidade.Controls.Add(this.txt_MSalarial);
            this.gpb_cidade.Controls.Add(this.lbl_MSalarial);
            this.gpb_cidade.Controls.Add(this.txt_MFporPessoa);
            this.gpb_cidade.Controls.Add(this.lbl_MFporPessoa);
            this.gpb_cidade.Controls.Add(this.txt_populacao);
            this.gpb_cidade.Controls.Add(this.lbl_populacao);
            this.gpb_cidade.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpb_cidade.Location = new System.Drawing.Point(163, 24);
            this.gpb_cidade.Name = "gpb_cidade";
            this.gpb_cidade.Size = new System.Drawing.Size(515, 359);
            this.gpb_cidade.TabIndex = 3;
            this.gpb_cidade.TabStop = false;
            this.gpb_cidade.Text = "Parnamirim";
            // 
            // lbl_MSalarial
            // 
            this.lbl_MSalarial.AutoSize = true;
            this.lbl_MSalarial.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_MSalarial.Location = new System.Drawing.Point(6, 89);
            this.lbl_MSalarial.Name = "lbl_MSalarial";
            this.lbl_MSalarial.Size = new System.Drawing.Size(112, 23);
            this.lbl_MSalarial.TabIndex = 4;
            this.lbl_MSalarial.Text = "Média Salarial:";
            // 
            // txt_MFporPessoa
            // 
            this.txt_MFporPessoa.BackColor = System.Drawing.Color.White;
            this.txt_MFporPessoa.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_MFporPessoa.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txt_MFporPessoa.Location = new System.Drawing.Point(389, 43);
            this.txt_MFporPessoa.Name = "txt_MFporPessoa";
            this.txt_MFporPessoa.ReadOnly = true;
            this.txt_MFporPessoa.Size = new System.Drawing.Size(67, 29);
            this.txt_MFporPessoa.TabIndex = 3;
            // 
            // lbl_MFporPessoa
            // 
            this.lbl_MFporPessoa.AutoSize = true;
            this.lbl_MFporPessoa.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_MFporPessoa.Location = new System.Drawing.Point(174, 45);
            this.lbl_MFporPessoa.Name = "lbl_MFporPessoa";
            this.lbl_MFporPessoa.Size = new System.Drawing.Size(209, 23);
            this.lbl_MFporPessoa.TabIndex = 2;
            this.lbl_MFporPessoa.Text = "Média de Filhos por Pessoa:";
            // 
            // txt_populacao
            // 
            this.txt_populacao.BackColor = System.Drawing.Color.White;
            this.txt_populacao.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_populacao.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txt_populacao.Location = new System.Drawing.Point(101, 43);
            this.txt_populacao.Name = "txt_populacao";
            this.txt_populacao.ReadOnly = true;
            this.txt_populacao.Size = new System.Drawing.Size(67, 29);
            this.txt_populacao.TabIndex = 1;
            // 
            // lbl_populacao
            // 
            this.lbl_populacao.AutoSize = true;
            this.lbl_populacao.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_populacao.Location = new System.Drawing.Point(6, 46);
            this.lbl_populacao.Name = "lbl_populacao";
            this.lbl_populacao.Size = new System.Drawing.Size(89, 23);
            this.lbl_populacao.TabIndex = 0;
            this.lbl_populacao.Text = "População:";
            // 
            // gpb_pessoa
            // 
            this.gpb_pessoa.BackColor = System.Drawing.Color.Transparent;
            this.gpb_pessoa.Controls.Add(this.label8);
            this.gpb_pessoa.Controls.Add(this.btn_limpar);
            this.gpb_pessoa.Controls.Add(this.btn_cadastrar);
            this.gpb_pessoa.Controls.Add(this.lbl_erro002);
            this.gpb_pessoa.Controls.Add(this.label7);
            this.gpb_pessoa.Controls.Add(this.rbtn_viuvo);
            this.gpb_pessoa.Controls.Add(this.rbtn_casado);
            this.gpb_pessoa.Controls.Add(this.rbtn_solteiro);
            this.gpb_pessoa.Controls.Add(this.label6);
            this.gpb_pessoa.Controls.Add(this.label5);
            this.gpb_pessoa.Controls.Add(this.label4);
            this.gpb_pessoa.Controls.Add(this.label3);
            this.gpb_pessoa.Controls.Add(this.label2);
            this.gpb_pessoa.Controls.Add(this.txt_numFilhos);
            this.gpb_pessoa.Controls.Add(this.txt_renda);
            this.gpb_pessoa.Controls.Add(this.txt_idade);
            this.gpb_pessoa.Controls.Add(this.txt_nome);
            this.gpb_pessoa.Controls.Add(this.txt_id);
            this.gpb_pessoa.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpb_pessoa.Location = new System.Drawing.Point(163, 23);
            this.gpb_pessoa.Name = "gpb_pessoa";
            this.gpb_pessoa.Size = new System.Drawing.Size(515, 426);
            this.gpb_pessoa.TabIndex = 4;
            this.gpb_pessoa.TabStop = false;
            this.gpb_pessoa.Text = "Pessoas";
            // 
            // btn_limpar
            // 
            this.btn_limpar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            this.btn_limpar.FlatAppearance.BorderSize = 0;
            this.btn_limpar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_limpar.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_limpar.ForeColor = System.Drawing.Color.White;
            this.btn_limpar.Location = new System.Drawing.Point(249, 354);
            this.btn_limpar.Name = "btn_limpar";
            this.btn_limpar.Size = new System.Drawing.Size(104, 37);
            this.btn_limpar.TabIndex = 17;
            this.btn_limpar.Text = "Limpar";
            this.btn_limpar.UseVisualStyleBackColor = false;
            this.btn_limpar.Click += new System.EventHandler(this.btn_limpar_Click);
            // 
            // btn_cadastrar
            // 
            this.btn_cadastrar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            this.btn_cadastrar.FlatAppearance.BorderSize = 0;
            this.btn_cadastrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_cadastrar.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_cadastrar.ForeColor = System.Drawing.Color.White;
            this.btn_cadastrar.Location = new System.Drawing.Point(371, 354);
            this.btn_cadastrar.Name = "btn_cadastrar";
            this.btn_cadastrar.Size = new System.Drawing.Size(104, 37);
            this.btn_cadastrar.TabIndex = 16;
            this.btn_cadastrar.Text = "Cadastrar";
            this.btn_cadastrar.UseVisualStyleBackColor = false;
            this.btn_cadastrar.Click += new System.EventHandler(this.btn_cadastrar_Click);
            // 
            // lbl_erro002
            // 
            this.lbl_erro002.AutoSize = true;
            this.lbl_erro002.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_erro002.ForeColor = System.Drawing.Color.Red;
            this.lbl_erro002.Location = new System.Drawing.Point(59, 114);
            this.lbl_erro002.Name = "lbl_erro002";
            this.lbl_erro002.Size = new System.Drawing.Size(81, 16);
            this.lbl_erro002.TabIndex = 15;
            this.lbl_erro002.Text = "* ID já existente";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(34, 266);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(120, 20);
            this.label7.TabIndex = 14;
            this.label7.Text = "Número de Filhos:";
            // 
            // rbtn_viuvo
            // 
            this.rbtn_viuvo.AutoSize = true;
            this.rbtn_viuvo.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtn_viuvo.Location = new System.Drawing.Point(303, 238);
            this.rbtn_viuvo.Name = "rbtn_viuvo";
            this.rbtn_viuvo.Size = new System.Drawing.Size(71, 24);
            this.rbtn_viuvo.TabIndex = 13;
            this.rbtn_viuvo.TabStop = true;
            this.rbtn_viuvo.Text = "Viúvo(a)";
            this.rbtn_viuvo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbtn_viuvo.UseVisualStyleBackColor = true;
            // 
            // rbtn_casado
            // 
            this.rbtn_casado.AutoSize = true;
            this.rbtn_casado.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtn_casado.Location = new System.Drawing.Point(212, 238);
            this.rbtn_casado.Name = "rbtn_casado";
            this.rbtn_casado.Size = new System.Drawing.Size(85, 24);
            this.rbtn_casado.TabIndex = 12;
            this.rbtn_casado.TabStop = true;
            this.rbtn_casado.Text = "Casado(a)";
            this.rbtn_casado.UseVisualStyleBackColor = true;
            // 
            // rbtn_solteiro
            // 
            this.rbtn_solteiro.AutoSize = true;
            this.rbtn_solteiro.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtn_solteiro.Location = new System.Drawing.Point(122, 238);
            this.rbtn_solteiro.Name = "rbtn_solteiro";
            this.rbtn_solteiro.Size = new System.Drawing.Size(84, 24);
            this.rbtn_solteiro.TabIndex = 11;
            this.rbtn_solteiro.TabStop = true;
            this.rbtn_solteiro.Text = "Solteiro(a)";
            this.rbtn_solteiro.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(34, 240);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(84, 20);
            this.label6.TabIndex = 10;
            this.label6.Text = "Estado Civil:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(159, 190);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 20);
            this.label5.TabIndex = 9;
            this.label5.Text = "Renda:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(39, 190);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 20);
            this.label4.TabIndex = 8;
            this.label4.Text = "Idade:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(37, 158);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 20);
            this.label3.TabIndex = 7;
            this.label3.Text = "Nome:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(24, 88);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(25, 20);
            this.label2.TabIndex = 6;
            this.label2.Text = "ID:";
            // 
            // txt_numFilhos
            // 
            this.txt_numFilhos.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_numFilhos.Location = new System.Drawing.Point(158, 263);
            this.txt_numFilhos.Name = "txt_numFilhos";
            this.txt_numFilhos.Size = new System.Drawing.Size(48, 26);
            this.txt_numFilhos.TabIndex = 5;
            // 
            // txt_renda
            // 
            this.txt_renda.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_renda.Location = new System.Drawing.Point(217, 190);
            this.txt_renda.Name = "txt_renda";
            this.txt_renda.Size = new System.Drawing.Size(258, 26);
            this.txt_renda.TabIndex = 3;
            // 
            // txt_idade
            // 
            this.txt_idade.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_idade.Location = new System.Drawing.Point(91, 187);
            this.txt_idade.Name = "txt_idade";
            this.txt_idade.Size = new System.Drawing.Size(62, 26);
            this.txt_idade.TabIndex = 2;
            // 
            // txt_nome
            // 
            this.txt_nome.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_nome.Location = new System.Drawing.Point(91, 155);
            this.txt_nome.Name = "txt_nome";
            this.txt_nome.Size = new System.Drawing.Size(384, 26);
            this.txt_nome.TabIndex = 1;
            // 
            // txt_id
            // 
            this.txt_id.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_id.Location = new System.Drawing.Point(55, 85);
            this.txt_id.Name = "txt_id";
            this.txt_id.Size = new System.Drawing.Size(85, 26);
            this.txt_id.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(12, 24);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(115, 98);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(24, 125);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 23);
            this.label1.TabIndex = 5;
            this.label1.Text = "SensoPop";
            // 
            // txt_MSalarial
            // 
            this.txt_MSalarial.BackColor = System.Drawing.Color.White;
            this.txt_MSalarial.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_MSalarial.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txt_MSalarial.Location = new System.Drawing.Point(124, 86);
            this.txt_MSalarial.Name = "txt_MSalarial";
            this.txt_MSalarial.ReadOnly = true;
            this.txt_MSalarial.Size = new System.Drawing.Size(67, 29);
            this.txt_MSalarial.TabIndex = 5;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(283, 219);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(192, 16);
            this.label8.TabIndex = 18;
            this.label8.Text = "(Digite apenas \",\" para casas decimais)";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(18, 126);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(172, 25);
            this.label9.TabIndex = 6;
            this.label9.Text = "Número de Pessoas";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(32, 151);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(136, 25);
            this.label10.TabIndex = 7;
            this.label10.Text = "por Estado Civil";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(19, 183);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(100, 23);
            this.label11.TabIndex = 8;
            this.label11.Text = "Solteiros(as):";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(19, 209);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(104, 23);
            this.label12.TabIndex = 9;
            this.label12.Text = "Casados(as):";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(19, 236);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(88, 23);
            this.label13.TabIndex = 10;
            this.label13.Text = "Viúvos(as):";
            // 
            // txt_PessoasSolteiros
            // 
            this.txt_PessoasSolteiros.BackColor = System.Drawing.Color.White;
            this.txt_PessoasSolteiros.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PessoasSolteiros.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txt_PessoasSolteiros.Location = new System.Drawing.Point(123, 182);
            this.txt_PessoasSolteiros.Name = "txt_PessoasSolteiros";
            this.txt_PessoasSolteiros.ReadOnly = true;
            this.txt_PessoasSolteiros.Size = new System.Drawing.Size(67, 29);
            this.txt_PessoasSolteiros.TabIndex = 11;
            // 
            // txt_PessoasCasados
            // 
            this.txt_PessoasCasados.BackColor = System.Drawing.Color.White;
            this.txt_PessoasCasados.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PessoasCasados.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txt_PessoasCasados.Location = new System.Drawing.Point(123, 208);
            this.txt_PessoasCasados.Name = "txt_PessoasCasados";
            this.txt_PessoasCasados.ReadOnly = true;
            this.txt_PessoasCasados.Size = new System.Drawing.Size(67, 29);
            this.txt_PessoasCasados.TabIndex = 12;
            // 
            // txt_PessoasViúvos
            // 
            this.txt_PessoasViúvos.BackColor = System.Drawing.Color.White;
            this.txt_PessoasViúvos.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_PessoasViúvos.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txt_PessoasViúvos.Location = new System.Drawing.Point(123, 234);
            this.txt_PessoasViúvos.Name = "txt_PessoasViúvos";
            this.txt_PessoasViúvos.ReadOnly = true;
            this.txt_PessoasViúvos.Size = new System.Drawing.Size(67, 29);
            this.txt_PessoasViúvos.TabIndex = 13;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(218, 138);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(156, 25);
            this.label14.TabIndex = 14;
            this.label14.Text = "Média Salarial por";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(414, 138);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(60, 25);
            this.label15.TabIndex = 15;
            this.label15.Text = "Filhos";
            // 
            // txt_Nfilhos
            // 
            this.txt_Nfilhos.BackColor = System.Drawing.Color.White;
            this.txt_Nfilhos.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Nfilhos.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txt_Nfilhos.Location = new System.Drawing.Point(374, 136);
            this.txt_Nfilhos.Name = "txt_Nfilhos";
            this.txt_Nfilhos.Size = new System.Drawing.Size(39, 29);
            this.txt_Nfilhos.TabIndex = 16;
            // 
            // btn_buscar
            // 
            this.btn_buscar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            this.btn_buscar.FlatAppearance.BorderSize = 0;
            this.btn_buscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_buscar.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_buscar.ForeColor = System.Drawing.Color.White;
            this.btn_buscar.Location = new System.Drawing.Point(374, 173);
            this.btn_buscar.Name = "btn_buscar";
            this.btn_buscar.Size = new System.Drawing.Size(85, 27);
            this.btn_buscar.TabIndex = 17;
            this.btn_buscar.Text = "Buscar";
            this.btn_buscar.UseVisualStyleBackColor = false;
            this.btn_buscar.Click += new System.EventHandler(this.btn_buscar_Click);
            // 
            // txt_MSporFilhos
            // 
            this.txt_MSporFilhos.BackColor = System.Drawing.Color.White;
            this.txt_MSporFilhos.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_MSporFilhos.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txt_MSporFilhos.Location = new System.Drawing.Point(274, 171);
            this.txt_MSporFilhos.Name = "txt_MSporFilhos";
            this.txt_MSporFilhos.ReadOnly = true;
            this.txt_MSporFilhos.Size = new System.Drawing.Size(73, 29);
            this.txt_MSporFilhos.TabIndex = 18;
            // 
            // frm_main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(700, 500);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.gpb_cidade);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.gpb_pessoa);
            this.Controls.Add(this.btn_sair);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximumSize = new System.Drawing.Size(700, 500);
            this.MinimumSize = new System.Drawing.Size(700, 500);
            this.Name = "frm_main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Senso Populacional";
            this.Load += new System.EventHandler(this.frm_main_Load);
            this.gpb_cidade.ResumeLayout(false);
            this.gpb_cidade.PerformLayout();
            this.gpb_pessoa.ResumeLayout(false);
            this.gpb_pessoa.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btn_sair;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.GroupBox gpb_cidade;
        private System.Windows.Forms.GroupBox gpb_pessoa;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_numFilhos;
        private System.Windows.Forms.TextBox txt_renda;
        private System.Windows.Forms.TextBox txt_idade;
        private System.Windows.Forms.TextBox txt_nome;
        private System.Windows.Forms.TextBox txt_id;
        private System.Windows.Forms.Label lbl_erro002;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.RadioButton rbtn_viuvo;
        private System.Windows.Forms.RadioButton rbtn_casado;
        private System.Windows.Forms.RadioButton rbtn_solteiro;
        private System.Windows.Forms.Button btn_limpar;
        private System.Windows.Forms.Button btn_cadastrar;
        private System.Windows.Forms.TextBox txt_populacao;
        private System.Windows.Forms.Label lbl_populacao;
        private System.Windows.Forms.TextBox txt_MFporPessoa;
        private System.Windows.Forms.Label lbl_MFporPessoa;
        private System.Windows.Forms.Label lbl_MSalarial;
        private System.Windows.Forms.TextBox txt_MSalarial;
        private System.Windows.Forms.TextBox txt_PessoasViúvos;
        private System.Windows.Forms.TextBox txt_PessoasCasados;
        private System.Windows.Forms.TextBox txt_PessoasSolteiros;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txt_MSporFilhos;
        private System.Windows.Forms.Button btn_buscar;
        private System.Windows.Forms.TextBox txt_Nfilhos;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
    }
}