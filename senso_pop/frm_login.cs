﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace senso_pop
{
    public partial class frm_login : Form
    {
        public frm_login()
        {
            InitializeComponent();
        }

        private void frm_login_Load(object sender, EventArgs e)
        {
            lbl_error001.Hide();
        }

        private void fecharToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ajudaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show(" Contate o administrador do sistema para obter ajuda! ", " Ajuda ");
        }

        private void sobreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Senso Populacional\n \nDesenvolvido por: Luciano Fernandes.\nContato: luciano.fernandesl@hotmail.com\n2017", "Sobre");
        }

        private void btn_logar_Click(object sender, EventArgs e)
        {
            if (validarUsuario(txt_user.Text.ToLower(), txt_senha.Text.ToLower()) == true)
            {
                frm_main main = new frm_main();
                main.Show();
                this.Hide();
                lbl_error001.Hide();
            }
            else
            {
                lbl_error001.Show();
            }
        }

        public Boolean validarUsuario (String usuario, String senha)
        {
            if (usuario.Equals("admin") && senha.Equals("admin"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
